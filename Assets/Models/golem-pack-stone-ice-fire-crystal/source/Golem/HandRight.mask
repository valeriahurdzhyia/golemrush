%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: HandRight
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Body
    m_Weight: 0
  - m_Path: EyeL
    m_Weight: 0
  - m_Path: EyesR
    m_Weight: 0
  - m_Path: HandsL_1
    m_Weight: 0
  - m_Path: HandsL_2
    m_Weight: 0
  - m_Path: HandsR_1
    m_Weight: 1
  - m_Path: HandsR_2
    m_Weight: 1
  - m_Path: Jaw
    m_Weight: 0
  - m_Path: root
    m_Weight: 0
  - m_Path: root/ik_foot_root
    m_Weight: 0
  - m_Path: root/ik_foot_root/ik_foot_l
    m_Weight: 0
  - m_Path: root/ik_foot_root/ik_foot_r
    m_Weight: 0
  - m_Path: root/ik_hand_root
    m_Weight: 1
  - m_Path: root/ik_hand_root/ik_hand_gun
    m_Weight: 1
  - m_Path: root/ik_hand_root/ik_hand_gun/ik_hand_l
    m_Weight: 0
  - m_Path: root/ik_hand_root/ik_hand_gun/ik_hand_r
    m_Weight: 1
  - m_Path: root/pelvis
    m_Weight: 0
  - m_Path: root/pelvis/spine_01
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/index_01_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/index_01_l/index_02_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/ring_01_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/ring_01_l/ring_02_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l/thumb_02_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l/thumb_02_l/thumb_03_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/lowerarm_twist_01_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/Stone1_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/Stone2_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/upperarm_twist_01_l
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/index_01_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/index_01_r/index_02_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/ring_01_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/ring_01_r/ring_02_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r/thumb_02_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r/thumb_02_r/thumb_03_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/lowerarm_twist_01_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/Stone1_R1
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/Stone2_R1
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/upperarm_twist_01_r
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/neck_01
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/neck_01/head
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/neck_01/head/unrealJaw1_M
    m_Weight: 1
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/Stone5_R1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/Stone6_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/Stone7_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/Stone8_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/spine_02/spine_03/Stone9_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/Stone10_L1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/Stone11_R1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/Stone12_R1
    m_Weight: 0
  - m_Path: root/pelvis/spine_01/Stone13_L1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/calf_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/calf_l/calf_twist_01_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/calf_l/foot_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/calf_l/foot_l/ball_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/calf_l/Stone17_L1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/Stone14_L1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/Stone15_L1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/Stone16_L1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_l/thigh_twist_01_l
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/calf_r
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/calf_r/calf_twist_01_r
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/calf_r/foot_r
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/calf_r/foot_r/ball_r
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/calf_r/Stone17_R1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/Stone14_R1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/Stone15_R1
    m_Weight: 0
  - m_Path: root/pelvis/thigh_r/Stone16_R1
    m_Weight: 1
  - m_Path: root/pelvis/thigh_r/thigh_twist_01_r
    m_Weight: 1
