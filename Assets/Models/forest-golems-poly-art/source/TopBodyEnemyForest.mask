%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: TopBodyEnemyForest
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: CG
    m_Weight: 0
  - m_Path: CG/ Pelvis
    m_Weight: 1
  - m_Path: CG/ Pelvis/ L Thigh
    m_Weight: 1
  - m_Path: CG/ Pelvis/ L Thigh/ L Calf
    m_Weight: 1
  - m_Path: CG/ Pelvis/ L Thigh/ L Calf/ L Foot
    m_Weight: 1
  - m_Path: CG/ Pelvis/ R Thigh
    m_Weight: 1
  - m_Path: CG/ Pelvis/ R Thigh/ R Calf
    m_Weight: 1
  - m_Path: CG/ Pelvis/ R Thigh/ R Calf/ R Foot
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger0
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger0/ L Finger01
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger0/ L Finger01/ L Finger02
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger1
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger1/ L Finger11
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger1/ L Finger11/ L Finger12
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger2
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger2/ L Finger21
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ L Clavicle/ L UpperArm/ L Forearm/
      L Hand/ L Finger2/ L Finger21/ L Finger22
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ Neck
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ Neck/ Head
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ Neck/ Head/ Jaw
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ Neck/ Head/ Jaw/Tongue
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger0
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger0/ R Finger01
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger0/ R Finger01/ R Finger02
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger1
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger1/ R Finger11
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger1/ R Finger11/ R Finger12
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger2
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger2/ R Finger21
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/ R Finger2/ R Finger21/ R Finger22
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/Prop1
    m_Weight: 1
  - m_Path: CG/ Pelvis/ Spine/ Spine1/ Spine2/ R Clavicle/ R UpperArm/ R Forearm/
      R Hand/Prop1/Golem_04_Polyart_Trunk02
    m_Weight: 1
  - m_Path: CG001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ L Thigh001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ L Thigh001/ L Calf001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ L Thigh001/ L Calf001/ L Foot001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ R Thigh001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ R Thigh001/ R Calf001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ R Thigh001/ R Calf001/ R Foot001
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ Spine003
    m_Weight: 0
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger023
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger023/ L Finger024
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger023/ L Finger024/ L Finger025
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger026
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger026/ L Finger027
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger026/ L Finger027/ L Finger028
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger029
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger029/ L Finger030
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ L Clavicle001/ L UpperArm001/
      L Forearm001/ L Hand001/ L Finger029/ L Finger030/ L Finger031
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ Neck001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ Neck001/ Head001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ Neck001/ Head001/ Jaw001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ Neck001/ Head001/ Jaw001/Tongue001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger023
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger023/ R Finger024
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger023/ R Finger024/ R Finger025
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger026
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger026/ R Finger027
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger026/ R Finger027/ R Finger028
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger029
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger029/ R Finger030
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/ R Finger029/ R Finger030/ R Finger031
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/Prop002
    m_Weight: 1
  - m_Path: CG001/ Pelvis001/ Spine003/ Spine004/ Spine005/ R Clavicle001/ R UpperArm001/
      R Forearm001/ R Hand001/Prop002/Golem_01_Polyart_Trunk006
    m_Weight: 1
  - m_Path: CG002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ L Thigh002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ L Thigh002/ L Calf002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ L Thigh002/ L Calf002/ L Foot002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ R Thigh002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ R Thigh002/ R Calf002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ R Thigh002/ R Calf002/ R Foot002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger032
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger032/ L Finger033
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger032/ L Finger033/ L Finger034
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger044
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger044/ L Finger045
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger044/ L Finger045/ L Finger046
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger047
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger047/ L Finger048
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ L Clavicle002/ L UpperArm002/
      L Forearm002/ L Hand002/ L Finger047/ L Finger048/ L Finger049
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ Neck002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ Neck002/ Head002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ Neck002/ Head002/ Jaw002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ Neck002/ Head002/ Jaw002/Tongue002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002
    m_Weight: 0
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger032
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger032/ R Finger033
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger032/ R Finger033/ R Finger034
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger044
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger044/ R Finger045
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger044/ R Finger045/ R Finger046
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger047
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger047/ R Finger048
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/ R Finger047/ R Finger048/ R Finger049
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/Prop004
    m_Weight: 1
  - m_Path: CG002/ Pelvis002/ Spine006/ Spine010/ Spine011/ R Clavicle002/ R UpperArm002/
      R Forearm002/ R Hand002/Prop004/Golem_01_Polyart_Trunk011
    m_Weight: 1
  - m_Path: CG003
    m_Weight: 0
  - m_Path: CG003/ Pelvis003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ L Thigh003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ L Thigh003/ L Calf003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ L Thigh003/ L Calf003/ L Foot003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ R Thigh003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ R Thigh003/ R Calf003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ R Thigh003/ R Calf003/ R Foot003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger035
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger035/ L Finger036
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger035/ L Finger036/ L Finger037
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger038
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger038/ L Finger039
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger038/ L Finger039/ L Finger040
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger041
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger041/ L Finger042
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ L Clavicle003/ L UpperArm003/
      L Forearm003/ L Hand003/ L Finger041/ L Finger042/ L Finger043
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ Neck003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ Neck003/ Head003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ Neck003/ Head003/ Jaw003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ Neck003/ Head003/ Jaw003/Tongue003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger035
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger035/ R Finger036
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger035/ R Finger036/ R Finger037
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger038
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger038/ R Finger039
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger038/ R Finger039/ R Finger040
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger041
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger041/ R Finger042
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/ R Finger041/ R Finger042/ R Finger043
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/Prop003
    m_Weight: 1
  - m_Path: CG003/ Pelvis003/ Spine007/ Spine008/ Spine009/ R Clavicle003/ R UpperArm003/
      R Forearm003/ R Hand003/Prop003/Golem_01_Polyart_Trunk007
    m_Weight: 1
  - m_Path: Golem_01_PA001
    m_Weight: 1
  - m_Path: Golem_02_PA
    m_Weight: 0
  - m_Path: Golem_03_PA002
    m_Weight: 0
  - m_Path: Golem_04_PA003
    m_Weight: 0
