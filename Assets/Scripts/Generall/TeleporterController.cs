﻿using System.Collections;
using System.Threading.Tasks;
using UnityEngine;


public class TeleporterController : MonoBehaviour
{
    [SerializeField] private Transform _startPoint;

    private Vector3 _destinationPosition;
    private Quaternion _destinationRotation;

    private void Awake()
    {
        if(_startPoint == null)
        {
            _destinationPosition = transform.position;
            _destinationRotation = transform.rotation;
        }
        else
        {
            _destinationPosition = _startPoint.position;
            _destinationRotation = _startPoint.rotation;
        }
    }

    public void Teleport(Rigidbody rigidbody)
    {
        //rigidbody.velocity = Vector3.zero;
        //rigidbody.angularDrag = 0;
        //rigidbody.angularVelocity = Vector3.zero;
        //rigidbody.drag = 0;

        rigidbody.Sleep();

        rigidbody.position = _destinationPosition;
        rigidbody.MoveRotation(_destinationRotation);


    }
}
