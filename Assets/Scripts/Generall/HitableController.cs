using MoreMountains.NiceVibrations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class HitableController : MonoBehaviour
{
    public Action OnHit;
    public Action OnDie;

    [SerializeField] private int _healthBase;

    [SerializeField] private ParticleSystem _effectEnemyDie;
    [SerializeField] private ParticleSystem _effectGolemHit;

    [SerializeField] private string _tagDamagable;
    [SerializeField] private HPController _hpController;
    [SerializeField] private float _delayBeforeResetInSeconds = 3f;

    private int _health;

    private AudioSource audioSource;
    
    private Coroutine _passiveDamageCoroutine;
    private Coroutine _resetWithDelayCoroutine;

    private float _passiveDamageDelayInSeconds = 1f;
    private int _delayBeforeDeadInMilliseconds = 500;
    private bool _isAttacked = false;

    private void Start()
    {
        _health = _healthBase;
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == _tagDamagable)
        {
            _isAttacked = true;
           DoDamage();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == _tagDamagable)
        {
            _isAttacked = false;
            StartResetWithDelay();
        }
    }

    public void StartPassiveDamage()
    {
        if (_passiveDamageCoroutine != null)
        {
            StopCoroutine(_passiveDamageCoroutine);
        }

        _passiveDamageCoroutine = StartCoroutine(PassiveDamageCoroutine());
    }

    public void StopPassiveDamage()
    {
        if(_passiveDamageCoroutine!=null)
        {
            StopCoroutine(_passiveDamageCoroutine);
            DoReset();
        }
    }

    private IEnumerator PassiveDamageCoroutine()
    {
        var delay = new WaitForSeconds(_passiveDamageDelayInSeconds);
        while(true)
        {
            yield return delay;
            DoDamage();
        }
    }

    public async void DoDamage()
    {
        StopResetWithDelay();

        if (_health > 0)
        {
            _health--;

            _hpController.ChangeHP(-1, _healthBase);

            audioSource.Play();

            _effectGolemHit.Play();
            OnHit?.Invoke();

            MMVibrationManager.Haptic(HapticTypes.HeavyImpact);
        }
        else if (_health == 0)
        {
            await Task.Delay(_delayBeforeDeadInMilliseconds);

            _effectEnemyDie.Play();
            OnDie?.Invoke();

            MMVibrationManager.Haptic(HapticTypes.Success);
        }
    }

    public void DoReset(bool fastFill = false)
    {
        if(!_isAttacked && _health < _healthBase)
        {
            _health = _healthBase;
            _hpController.ChangeHP(_healthBase, _healthBase, fastFill);
        }       
    }

    private IEnumerator ResetWithDelay()
    {
        yield return new WaitForSeconds(_delayBeforeResetInSeconds);
        DoReset();
    }

    private void StartResetWithDelay()
    {
        StopResetWithDelay();

        _resetWithDelayCoroutine = StartCoroutine(ResetWithDelay());
    }

    private void StopResetWithDelay()
    {
        if(_resetWithDelayCoroutine!=null)
        {
            StopCoroutine(_resetWithDelayCoroutine);
        }
    }
}
