using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class GolemUpgrader : MonoBehaviour, IEventListener<PlayerState>
{
    [SerializeField] private PlayerState _playerState;
    [SerializeField] private OnPlayerChangeStateEvent _onPlayerChangeStateEvent;
    [SerializeField] private UIThrowResources _uIThrowResources;

    [SerializeField] private float _delayBeforeUpgradeInSeconds;
    private int _delayBeforeUpgradeInMilliseconds;

    void Awake()
    {
        _delayBeforeUpgradeInMilliseconds = (int)_delayBeforeUpgradeInSeconds * 1000;
    }

    void OnEnable()
    {
        _onPlayerChangeStateEvent.RegisterListener(this);
    }

    void OnDisable()
    {
        _onPlayerChangeStateEvent.UnregisterListener(this);
    }

    public void OnEventRaised(PlayerState eventData)
    {
        if(eventData!=_playerState)
        {
            _uIThrowResources.Restore();
        }
    }

    public void OnEventRaised()
    {
        
    }

    public async void UpgradeCharacter()
    {
        await Task.Delay(_delayBeforeUpgradeInMilliseconds);
        _onPlayerChangeStateEvent.Raise(_playerState);
    }
}
