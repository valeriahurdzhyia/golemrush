using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TerritoryData", menuName = "Territory/TerritoryData")]
public class TerritoryData : ScriptableObject
{
    [SerializeField] private Material _territoryOpenMaterial;
    [SerializeField] private Material _territoryCloseMaterial;
    [SerializeField] private float _heightOpen = 0.2f;
    [SerializeField] private float _heightClose = 0f;
    [SerializeField] private PlayerState _playerState;

    public Material TerritoryOpenMaterial => _territoryOpenMaterial;
    public Material TerritoryCloseMaterial => _territoryCloseMaterial;
    public float HeightOpen => _heightOpen;
    public float HeightClose => _heightClose;

    public PlayerState PlayerState => _playerState;
}
