using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerritoryHolder : MonoBehaviour
{
    private TerritoryObject[] _territoryObjectArray;

    public void SetActive(bool isActive)
    {
        if(_territoryObjectArray == null || _territoryObjectArray.Length == 0)
        {
            _territoryObjectArray = GetComponentsInChildren<TerritoryObject>();
        }
        foreach(var territoryObject in _territoryObjectArray)
        {
            territoryObject.SetActive(isActive);
        }
    }
}
