using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

public class TerritoryOpener : MonoBehaviour
{
    [SerializeField] private Tile _tile;
    [SerializeField] private GameObject _body;
    [SerializeField] private TerritoryData _territoryData;

    [SerializeField] private int _delayBeforeOpenInSeconds = 1;
    private int _delayBeforeOpenInMilliseconds;

    //[InspectorButton("OpenTerritory")]
    //public bool openTerritoryEditorButton;

    //[InspectorButton("CloseTerritory")]
    //public bool closeTerritoryEditorButton;
    private TerritoryHolder _territoryHolder;
    private void Awake()
    {
        _delayBeforeOpenInMilliseconds = _delayBeforeOpenInSeconds * 1000;
        _territoryHolder = _body.GetComponent<TerritoryHolder>();
    }

    public async void OpenTerritory()
    {
        float HeighStep = 0.5f;
        _tile.transform.position = new Vector3(_tile.transform.position.x, _tile.transform.position.y - HeighStep, _tile.transform.position.z);

        await Task.Delay(_delayBeforeOpenInMilliseconds);

        _tile.gameObject.AddComponent<TerritoryDataController>().territoryData = _territoryData;

        _tile.transform.DOMoveY(_tile.transform.position.y + HeighStep, 1);

        _tile.SetExtrusionHeight(_territoryData.HeightOpen);
        _tile.SetMaterial(_territoryData.TerritoryOpenMaterial);

        _tile.transform.DOMoveY(_tile.transform.position.y + HeighStep, 1).OnComplete(() =>
        {
            _body.SetActive(true);
            _territoryHolder.SetActive(true);
        });
    }

    public void CloseTerritory()
    {
        _tile.SetExtrusionHeight(_territoryData.HeightClose);
        _body.SetActive(false);
        _tile.SetMaterial(_territoryData.TerritoryCloseMaterial);
    }
}
