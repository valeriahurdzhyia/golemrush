using DG.Tweening;
using System.Threading.Tasks;
using UnityEngine;

public class TerritoryObject : MonoBehaviour
{   
    [SerializeField] private float _animationDurationInSeconds = 1;
    [SerializeField] private bool _isVibrationEnabled = true;
    
    private Ease _ease = Ease.InOutElastic;
    private Vector3 _defaultScale;

    public void SetActive(bool isActive)
    {
        if(_defaultScale == Vector3.zero)
        {
            _defaultScale = transform.localScale;
        }

        float durationAnimation = _animationDurationInSeconds + Random.Range(-0.1f, 0.1f);

        transform.localScale = Vector3.zero;
        transform.DOScale(_defaultScale, durationAnimation).SetEase(_ease);
    }
}
