using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class HPController : MonoBehaviour
{ 
    [SerializeField] private SpriteRenderer _hpFiller;

    [SerializeField] private float _hpChangeStep = 0.1f;
    [SerializeField] private float _hpChangeDelayInSecondsStep = 0.01f;
    [SerializeField] private float _delayInSecondsBeforeDisableUI = 1f;
    [SerializeField] private float _testValue = 10f;
    [SerializeField] private GameObject _holder;


    private float _maxValue;
    private Coroutine _hpFillCoroutine;

    private void Awake()
    {
        _maxValue = _hpFiller.size.x;
    }

    //Set parallel to the camera
    public Transform cam;
    void LateUpdate()
    {
        transform.LookAt(transform.position + cam.forward);
        transform.rotation = cam.transform.rotation;
    }

    //private void OnEnable()
    //{
    //    ChangeHP(_testValue, 10);
    //}

    private IEnumerator HPFillCoroutine(float changeValue)
    {
        _holder.SetActive(true);

        var goalValue = _hpFiller.size.x + changeValue;
        var delay = new WaitForSeconds(_hpChangeDelayInSecondsStep);

        if (changeValue < 0)
        {
            while (_hpFiller.size.x > goalValue && _hpFiller.size.x > 0)
            {
                _hpFiller.size -= new Vector2(_hpChangeStep, 0f);
                yield return delay;               
            }          
        }
        else
        {
            while (_hpFiller.size.x <= goalValue && _hpFiller.size.x < _maxValue)
            {
                _hpFiller.size += new Vector2(_hpChangeStep, 0f);
                yield return delay;
            }
        }

        yield return new WaitForSeconds(_delayInSecondsBeforeDisableUI);
        _holder.SetActive(false);
    }

    public async void FastHPChangeAsync(float changeValue)
    {
        _holder.SetActive(true);


        var goalValue = Mathf.Clamp(_hpFiller.size.x + changeValue, 0, _maxValue);
        float y = _hpFiller.size.y;
        _hpFiller.size = new Vector2(goalValue, y);

        await Task.Delay(1000);

        _holder.SetActive(false);
    }

    public void ChangeHP(float changeValue, float maxValue, bool fastFill = false)
    {
        
        if (_hpFillCoroutine != null)
        {
            StopCoroutine(_hpFillCoroutine);
        }

        float tryChangeValue = (_maxValue * changeValue) / maxValue;

        if (fastFill)
        {
            FastHPChangeAsync(tryChangeValue);
        }
        else
        {
            
            _hpFillCoroutine = StartCoroutine(HPFillCoroutine(tryChangeValue));
        }       
    }
}
