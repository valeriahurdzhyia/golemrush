using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ResetLevel : MonoBehaviour
{
    [SerializeField] private InventoryObject _invetrory;
    public void DoResetLevel()
    {
        _invetrory.DeleteAll();
        SceneManager.LoadScene(0);
    }
}
