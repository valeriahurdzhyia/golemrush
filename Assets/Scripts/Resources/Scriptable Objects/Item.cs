using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

public class Item : MonoBehaviour
{
    public const string TAG_RESOURCE = "Resource";

    public ItemObject item;
    private AudioSource audioSource;

    [SerializeField] private int _delayBeforeRestoreInSeconds = 2;
    private int _delayBeforeRestoreInMilliseconds;

    private FauxGravityAttractor _attractor;
    private ParticleSystem[] _collactableEffectArray;
    private MeshRenderer[] _meshRenderArray;
    private Collider _collider;
    private bool _isUsed = false;
    private Vector3 _defaultScale;
    private int _delayBeforeHideInMilliseconds = 250;

    private void Awake()
    {
        _attractor = FindObjectOfType<FauxGravityAttractor>();
        _collactableEffectArray = GetComponentsInChildren<ParticleSystem>(true);
        _meshRenderArray = GetComponentsInChildren<MeshRenderer>(true);
        _collider = GetComponent<Collider>();

        _delayBeforeRestoreInMilliseconds = _delayBeforeRestoreInSeconds * 1000;
        _defaultScale = transform.localScale;
    }
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public async void PlayEffect()
    {
        
        if (_isUsed)
        {
            return;
        }

        await Task.Delay(_delayBeforeHideInMilliseconds);

        int length = _collactableEffectArray.Length;
        for (int i = 0; i < length; i++)
        {
           // _collactableEffectArray[i].transform.SetParent(null);
            _collactableEffectArray[i].Play();
        }

        for (int i = 0; i < _meshRenderArray.Length; i++)
        {
            _meshRenderArray[i].enabled = false;
        }

        //Play AUDIO
        audioSource.Play();

       _collider.enabled = false;
        _isUsed = true;
        
       // await Task.Delay(1000);

       // _onPlayerChangePhaseEvent?.Raise(PlayerPhase.Idle);

        await Task.Delay(_delayBeforeRestoreInMilliseconds);

        
        transform.localScale = Vector3.zero;

        for (int i = 0; i < _meshRenderArray.Length; i++)
        {
            _meshRenderArray[i].enabled = true;
        }

        transform.DOScale(_defaultScale, 1).SetEase(Ease.InOutElastic).OnComplete(()=>
            {
                _isUsed = false;
                _collider.enabled = true;
            });
    }
}

