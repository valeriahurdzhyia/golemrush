using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Mineral Object", menuName = "Inventory System/Items/Mineral")]
public class MineralObject : ItemObject
{
    public void Awake()
    {
        type = ItemType.Mineral;
    }
}
