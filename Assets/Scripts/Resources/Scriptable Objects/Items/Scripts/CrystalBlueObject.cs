using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Crystal Blue Object", menuName = "Inventory System/Items/Crystal Blue")]
public class CrystalBlueObject : ItemObject
{
    public void Awake()
    {
        type = ItemType.CrystalBlue;
    }
}
