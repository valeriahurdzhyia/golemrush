using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Crystal Red Object", menuName = "Inventory System/Items/Crystal Red")]
public class CrystalRedObject : ItemObject
{
    public void Awake()
    {
        type = ItemType.CrystalRed;
    }
}