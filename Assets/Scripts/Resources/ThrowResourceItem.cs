using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ThrowResourceItem : MonoBehaviour
{
    [SerializeField] private ItemType _resourceType;
    public ItemType ResourceType => _resourceType;

    [SerializeField] private int _resourceCount;
    public int ResourceCount => _resourceCount;

    [SerializeField] private ResourceView _resourceView;
    public ResourceView ResourceView => _resourceView;

    private Coroutine _resourceThrowCoroutine;

    [SerializeField] private ParticleSystem _particleSystem;

    private int _currentResourceCount = 0;
    public int CurrentResourceCount => _currentResourceCount;

    public bool IsFull => CurrentResourceCount == ResourceCount;

    private InventoryObject _inventory;
    private Vector3 _punchScale;

    public void Intialize(InventoryObject inventory)
    {
        if (IsFull)
        {
            gameObject.SetActive(false);
        }

        _inventory = inventory;
        ResourceView.Initialize(_resourceType, CounterText());
        _punchScale = new Vector3(0.1f, 0.1f, 0.1f);
    }

    private void SetUpParticleSystemDuration(int durationInSeconds)
    {
        if(!_particleSystem.isPlaying)
        {
            var main = _particleSystem.main;
            main.duration = durationInSeconds * (ResourceCount - CurrentResourceCount);
        }
        
    }

    public void StopAndRestart(int durationEffectInSeconds, float delayBetweenUpdatesInSeconds, Action OnFinish)
    {
        if(IsFull)
        {
            OnFinish?.Invoke();
            return;
        }

        if (_resourceThrowCoroutine != null)
        {
            StopCoroutine(_resourceThrowCoroutine);
        }

        if (CurrentResourceCount >= ResourceCount)
        {
            _particleSystem.Stop();
        }
        else
        {
            SetUpParticleSystemDuration(durationEffectInSeconds);
            _resourceThrowCoroutine = StartCoroutine(ResourceThrowCoroutine(delayBetweenUpdatesInSeconds, OnFinish));
        }
    }

    public void Stop()
    {
        if (_resourceThrowCoroutine != null)
        {
            StopCoroutine(_resourceThrowCoroutine);
        }
        _particleSystem.Stop();
    }

    public void DoRestart()
    {
        _currentResourceCount = 0;
        _resourceView.TextCountResource.text = CounterText();
    }

    private string CounterText()
    {
        return CurrentResourceCount.ToString() + "/" + ResourceCount.ToString();
    }

    private IEnumerator ResourceThrowCoroutine(float delayInSeconds, Action OnFinish)
    {
        var itemSlot = _inventory.Container.Find(x => x.item.type == _resourceType);
        var delay = new WaitForSeconds(delayInSeconds);

        if (itemSlot != null)
        {
            if(CurrentResourceCount < ResourceCount)
            {
                _particleSystem.Play();
            }
            
            var amount = itemSlot.amount;

            while (CurrentResourceCount < ResourceCount && amount > 0)
            {
                yield return delay;
                amount--;
                _inventory.RemoveItem(itemSlot.item, 1);
                _currentResourceCount++;
                
                _resourceView.TextCountResource.text = CounterText();

                transform.DOPunchScale(_punchScale, delayInSeconds, 5);
            }

            if (CurrentResourceCount == ResourceCount)
            {
                OnFinish?.Invoke();
            }

            _particleSystem.Stop();
        }
    }
}
