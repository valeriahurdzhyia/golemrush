using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceSound : MonoBehaviour
{
    private AudioSource audioSource;
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider collision)
    {
        if(collision.tag == PlayerController.PLAYER_AUDIOTRACKER_TAG)
        {
            audioSource.Play();
        }
    }
}
