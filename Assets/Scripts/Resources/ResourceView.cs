using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourceView : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _icon;
    [SerializeField] private Sprite _woodSprite;
    [SerializeField] private Sprite _crystalSprite;
    [SerializeField] private Sprite _rockSprite;

    [SerializeField] private TextMeshPro _textCountResource;
    public TextMeshPro TextCountResource => _textCountResource;

    public void Initialize(ItemType type, string countString)
    {
        switch(type)
        {
            case ItemType.Wood:
                _icon.sprite = _woodSprite;
                _icon.color = Color.white;
                break;
            case ItemType.CrystalRed:
                _icon.sprite = _crystalSprite;
                _icon.color = Color.red;
                break;
            case ItemType.CrystalBlue:
                _icon.sprite = _crystalSprite;
                _icon.color = Color.blue;
                break;
            case ItemType.Mineral:
                _icon.sprite = _rockSprite;
                _icon.color = Color.white;
                break;
        }

        _textCountResource.text = countString;
    }
}
