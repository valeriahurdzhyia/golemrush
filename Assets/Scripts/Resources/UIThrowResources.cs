using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine.Events;
using System.Linq;

public class UIThrowResources : MonoBehaviour
{
    public UnityEvent OnResorcesIsFilled;
    public Transform ResourseHolder;

    [SerializeField] private int _delayBeforeRestoreInSeconds = 2;
    private int _delayBeforeRestoreInMilliseconds;

    [SerializeField] private InventoryObject _inventory;
    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private float _throwResourcesDelayInSeconds = 0.1f;
    [SerializeField] private float _scaleResourceHolderDurationInSeconds = 1;
    [SerializeField] private GameObject _interractiveZone;
    [SerializeField] private ThrowResourceItem[] _throwResourceItemArray;

    
    private Vector3 _directionToLook;
    private Transform _cameraTransform;
    private bool _isUsed = false;
    private Vector3 _defaultGenerallScale;
    private Vector3 _defaultInterractiveZonelScale;
    private int _fillCounter = 0;
    private int _currentFillCounter = 0;

    private void Start()
    {
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _particleSystem.Stop();

        foreach(var resourceItem in _throwResourceItemArray)
        {
            if(!resourceItem.IsFull)
            {
                _fillCounter++;
            }
            resourceItem.Intialize(_inventory);
        }

        _cameraTransform = Camera.main.transform;
        _delayBeforeRestoreInMilliseconds = _delayBeforeRestoreInSeconds * 1000;
        _defaultGenerallScale = transform.localScale;
        _defaultInterractiveZonelScale = _interractiveZone.transform.localScale;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == PlayerController.PLAYER_TAG) 
        {
            if(_isUsed)
            {
                return;
            }

            foreach (var resourceItem in _throwResourceItemArray)
            {
                if(!resourceItem.IsFull)
                {
                    resourceItem.StopAndRestart(
                    _delayBeforeRestoreInSeconds,
                    _throwResourcesDelayInSeconds,
                    () =>
                    {
                        OnFill();
                    });
                } 
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == PlayerController.PLAYER_TAG)
        {
            if (_isUsed)
            {
                _isUsed = false;
            }

            foreach (var resourceItem in _throwResourceItemArray)
            {
                resourceItem.Stop();
            }
        }
    }

    private void OnFill()
    {
        _currentFillCounter++;

        if(_fillCounter == _currentFillCounter)
        {
            _interractiveZone.transform.DOScale(0, _scaleResourceHolderDurationInSeconds);
            ResourseHolder.DOScale(0, _scaleResourceHolderDurationInSeconds);
            OnResorcesIsFilled?.Invoke();
            _isUsed = true;
        }
    }

    private void Update()
    {
        //_directionToLook = _cameraTransform.position - ResourseHolder.position;
        //var newRotation = Quaternion.LookRotation(_directionToLook) * Quaternion.Euler(0, 90, 0);
        //ResourseHolder.rotation = Quaternion.Slerp(ResourseHolder.rotation, newRotation, Time.deltaTime * 1.0f);
    }

    public async void Restore()
    {
        if (_delayBeforeRestoreInSeconds >= 0)
        {
            await Task.Delay(_delayBeforeRestoreInMilliseconds);

            foreach (var resourceItem in _throwResourceItemArray)
            {
                resourceItem.DoRestart();
            }

            _interractiveZone.transform.DOScale(_defaultInterractiveZonelScale, _scaleResourceHolderDurationInSeconds);
            ResourseHolder.DOScale(_defaultGenerallScale, _scaleResourceHolderDurationInSeconds).OnComplete(() =>
            {
                _isUsed = false;
            });
        }
    }

    
}
