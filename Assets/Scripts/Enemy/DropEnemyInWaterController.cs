using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DropEnemyInWaterController : DropInWaterController
{
    public override async void DropInWater(TeleporterController teleporter)
    {
        _effectDrownInWater.transform.SetParent(null);
        _effectDrownInWater.transform.localEulerAngles = _dropInWaterAngle;
        _effectDrownInWater.Play();

        Collider.isTrigger = true;

        await Task.Delay(_delayInMilliseconds);

        Rigidbody.velocity = Vector3.zero;

        teleporter.Teleport(Rigidbody);

        Collider.isTrigger = false;

        await Task.Delay(_delayInMilliseconds);

        _effectDrownInWater.transform.SetParent(transform);
        _effectDrownInWater.transform.localPosition = _startParticleLocalPosition;
        _effectDrownInWater.transform.localEulerAngles = _startParticleLocalAngle;
    }
}
