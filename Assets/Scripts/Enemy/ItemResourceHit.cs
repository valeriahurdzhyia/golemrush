using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemResourceHit : MonoBehaviour
{
    public ItemObject item;
    public InventoryObject inventory;
    public HitableController hitableController;
    private void OnEnable()
    {
        hitableController.OnHit += PlayEffect;
    }
    private void OnDisable()
    {
        hitableController.OnHit -= PlayEffect;
    }
    private void PlayEffect()
    {
        inventory.AddItem(item, 1);
        //add resource play animation
    }
}
