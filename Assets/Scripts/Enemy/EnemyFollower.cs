using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollower : MonoBehaviour
{
    private Vector3 _destinationPosition;
    private Quaternion _destinationRotation;

    private void Awake()
    {
        _destinationPosition = transform.position;
        _destinationRotation = transform.rotation;
    }
}
