using System.Threading.Tasks;
using UnityEngine;
using DG.Tweening;

public class EnemyController : CharacterController
{
    public const string ENEMY_TAG = "Enemy";

    [SerializeField] private Renderer _renderer;

    [SerializeField] private Collider _handLeft;
    [SerializeField] private Collider _handRight;

    [SerializeField] private EnemyFollower _follower;

    private Transform _playerTransform;
    private bool _isAlive = true;
    public bool IsAlive => _isAlive;

    private bool _nearThePlayer = false;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override async void DoReset()
    {
        _isAlive = false;

        _moveDir = Vector3.zero;
        _playerTransform = null;
        _characterAnimationController.DisableAttack();
        _handLeft.enabled = false;
        _handRight.enabled = false;

        _renderer.enabled = false;
        _collider.enabled = false;

        await Task.Delay(_delayBeforeRestoreInMilliseconds);

        _collider.enabled = true;
        _teleporter.Teleport(_rigidbody);
        _hitableController.DoReset();
        _isAlive = true;
        _renderer.enabled = true;
        _nearThePlayer = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_isAlive)
        {
            return;
        }

        if (other.tag == PlayerController.PLAYER_TAG)
        {
            _characterAnimationController.EnableAttack();
            _handLeft.enabled = true;
            _handRight.enabled = true;
            _characterAnimationController.StopWalk();
            _nearThePlayer = true;
        }
        else if(other.tag == PlayerController.PLAYER_TAG_VISIBLE && !_nearThePlayer)
        {
            _characterAnimationController.Walk();

            if(_playerTransform == null)
            {
                _playerTransform = other.transform;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!_isAlive)
        {
            return;
        }

        if (other.tag == PlayerController.PLAYER_TAG)
        {
            _characterAnimationController.DisableAttack();
            _handLeft.enabled = false;
            _handRight.enabled = false;
            _nearThePlayer = false;
        }
        else if (other.tag == PlayerController.PLAYER_TAG_VISIBLE)
        {
            _characterAnimationController.StopWalk();
            _playerTransform = null;
        }
    }

    private void Rotate()
    {
        transform.LookAt(_playerTransform, transform.up);
    }

    protected override void SetUpMoveDirection()
    {
        base.SetUpMoveDirection();
        
        if(!_isAlive)
        {
            return;
        }

        if(_playerTransform != null && !_nearThePlayer)
        {
            _moveDir = (transform.position-_playerTransform.position).normalized;
            _moveDir = new Vector3(_moveDir.x, 0f, _moveDir.z);
            Rotate();
        }       
        else
        {
            _moveDir = Vector3.zero;
        }
    }
}
