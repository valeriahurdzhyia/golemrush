using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    public float moveSpeed = 5;

    [SerializeField] protected CharacterAnimationController _characterAnimationController;
    [SerializeField] protected HitableController _hitableController;
    [SerializeField] protected DropInWaterController _dropInWaterController;

    [SerializeField] protected int _delayBeforeRestoreInSeconds = 2;
    protected int _delayBeforeRestoreInMilliseconds;

    [SerializeField] protected Rigidbody _rigidbody;
    [SerializeField] protected Collider _collider;
    [SerializeField] protected TeleporterController _teleporter;

    [SerializeField] protected CharacterJumpController _characterJumpController;

    protected Vector3 _moveDir;
    protected bool _isGoing = false;

    protected string _waterTag = "Water";
    protected string _groundTag = "Ground";

    protected virtual void Awake()
    {
        _delayBeforeRestoreInMilliseconds = _delayBeforeRestoreInSeconds * 1000;

        _dropInWaterController.Collider = _collider;
        _dropInWaterController.Rigidbody = _rigidbody;
    }

    protected virtual void OnEnable()
    {
        _hitableController.OnDie += DoReset;
    }

    protected virtual void OnDisable()
    {
        _hitableController.OnDie -= DoReset;
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == _waterTag)
        {
            if(_dropInWaterController!=null)
            {
                _dropInWaterController.DropInWater(_teleporter);
            }
        }
        if(collision.transform.tag == _groundTag)
        {
            if(_characterJumpController!=null)
            {
                _characterJumpController.DoJumpIfPossible(_rigidbody, collision);
            }
        }
    }

    protected virtual void SetUpMoveDirection()
    {

    }

    protected virtual void Update()
    {
        SetUpMoveDirection();

        if (_moveDir == Vector3.zero)
        {
            if (_isGoing)
            {
                _characterAnimationController.StopWalk();
                _isGoing = false;
            }
        }
        else
        {
            if (!_isGoing)
            {
                _characterAnimationController.Walk();               
                _isGoing = true;
            }
        }
    }

    protected void FixedUpdate()
    {
        _rigidbody.MovePosition(_rigidbody.position + transform.TransformDirection(_moveDir) * moveSpeed * Time.deltaTime);
    }

    protected virtual async void DoReset()
    {       
        await Task.Delay(_delayBeforeRestoreInMilliseconds);
        
        
        _teleporter.Teleport(_rigidbody);
        _hitableController.StopPassiveDamage();

        await Task.Delay(250);

        _hitableController.DoReset(true);
        
    }
}
