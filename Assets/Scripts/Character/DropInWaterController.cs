using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DropInWaterController : MonoBehaviour
{
    [SerializeField] protected ParticleSystem _effectDrownInWater;
    
    public Collider Collider { get; set; }
    public Rigidbody Rigidbody { get; set; }
    
    protected Vector3 _dropInWaterAngle = new Vector3(90,180,0);

    protected Vector3 _startParticleLocalPosition;
    protected Vector3 _startParticleLocalAngle;
    protected int _delayInMilliseconds = 1000;

    private void Awake()
    {
        _startParticleLocalPosition = _effectDrownInWater.transform.localPosition;
        _startParticleLocalAngle = _effectDrownInWater.transform.localEulerAngles;
    }

    public virtual void DropInWater(TeleporterController teleporter)
    {
        
    }
}
