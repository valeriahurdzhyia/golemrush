using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJumpController : MonoBehaviour
{
    private Vector3 _jumpForce = new Vector3(0,2.0f,0);
    private int _currentHeight = 20;

    public void DoJumpIfPossible(Rigidbody rigidbody, Collision collision)
    {
        int height = (int)(collision.gameObject.GetComponent<Tile>().ExtrudedHeight * 1000);
        if (height > _currentHeight)
        {
            rigidbody.AddRelativeForce(_jumpForce, ForceMode.Impulse);
        }

        _currentHeight = height;
    }
}
