using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class CharacterAnimationController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    private string _walkParametr = "walk";
    private string _collectAnimation = "Collect";
    public void Walk()
    {
        _animator.SetBool(_walkParametr, true);
    }

    public void StopWalk()
    {
        _animator.SetBool(_walkParametr, false);
    }

    public void EnableAttack()
    {
        _animator.SetLayerWeight(1, 1);
    }

    public void DisableAttack()
    {
        _animator.SetLayerWeight(1, 0);
    }

    public void DoCollect()
    {
        _animator.Play(_collectAnimation, 2);
    }
}
