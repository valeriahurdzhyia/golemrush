using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public GameObject target;
    public float xSpeed = 3.5f;
    float sentivity = 17f;

    float minFov = 35;
    float maxFov = 100;

    private bool _isActive = true;

    private Vector3 _defaultLocalPosition;
    private Quaternion _defaultLocalRotation;

    private void Start()
    {
        _defaultLocalPosition = transform.localPosition;
        _defaultLocalRotation = transform.localRotation;
    }

    void LateUpdate()
    {
        if(!_isActive)
        {
            return;
        }

        if(Input.touchCount > 1)
        {
            return;
        }

        if (Input.GetMouseButton(1))
        {
            //transform.RotateAround(target.transform.position, transform.up, Input.GetAxis("Mouse X") * xSpeed);
            transform.RotateAround(target.transform.position, transform.right, -Input.GetAxis("Mouse Y") * xSpeed);
        }

        // ZOOM

        //float fov = Camera.main.fieldOfView;
        //fov += Input.GetAxis("Mouse ScrollWheel") * -sentivity;
        //fov = Mathf.Clamp(fov, minFov, maxFov);
        //Camera.main.fieldOfView = fov;
    }

    public void SetActive(bool isActive)
    {
        _isActive = isActive;
        if(_isActive)
        {
            transform.SetParent(target.transform);

            transform.localPosition = _defaultLocalPosition;
            transform.localRotation = _defaultLocalRotation;
        }
        else
        {
            transform.SetParent(null);
        }
    }
}
