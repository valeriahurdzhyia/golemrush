using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OnPlayerHPChangeEvent", menuName = "Event/OnPlayerHPChangeEvent")]
public class OnPlayerHPChangeEvent : ScriptableObjectEvent<float>
{

}
