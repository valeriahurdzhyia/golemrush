
using UnityEngine;

[CreateAssetMenu(fileName = "OnPlayerChangePhaseEvent", menuName = "Event/OnPlayerChangePhaseEvent")]
public class OnPlayerChangePhaseEvent : ScriptableObjectEvent<PlayerPhase>
{
    
}
