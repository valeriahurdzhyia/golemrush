public interface IEventListener<T>
{
    void OnEventRaised(T eventData);
    void OnEventRaised();
}
