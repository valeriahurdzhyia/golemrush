using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ScriptableObjectEvent<T> : ScriptableObject, IEvent<T>
{
	private List<IEventListener<T>> _listeners = new List<IEventListener<T>>();

	public void Raise(T data)
	{
		for (int i = _listeners.Count - 1; i >= 0; i--)
        {
			_listeners[i].OnEventRaised(data);
		}			
	}

    public void Raise()
    {
		for (int i = _listeners.Count - 1; i >= 0; i--)
		{
			_listeners[i].OnEventRaised();
		}
	}

    public void RegisterListener(IEventListener<T> listener)
	{ 
		_listeners.Add(listener); 
	}

	public void UnregisterListener(IEventListener<T> listener)
	{ 
		_listeners.Remove(listener); 
	}
}
