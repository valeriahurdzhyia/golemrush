public interface IEvent<T>
{
	void Raise(T data);
	void Raise();
	void RegisterListener(IEventListener<T> listener);
	void UnregisterListener(IEventListener<T> listener);
}
