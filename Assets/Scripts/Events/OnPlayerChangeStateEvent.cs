using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OnPlayerChangeStateEvent", menuName = "Event/OnPlayerChangeStateEvent")]
public class OnPlayerChangeStateEvent : ScriptableObjectEvent<PlayerState>
{

}
