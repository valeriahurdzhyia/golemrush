using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{

    [SerializeField] private float _health;
    [SerializeField] private float _scale;
    [SerializeField] private float _damage;

    public float Health => _health;
    public float Scale => _scale;
    public float Damage => _damage;

    public void UpdateStats(InventoryObject inventory)
    {
        foreach(var slot in inventory.Container)
        {
            if(slot.item.type == ItemType.Mineral && slot.amount > 0)
            {
                //find a rock
                inventory.RemoveItem(slot.item, 1);
                _health += 1;
                _scale += 1;
                _damage += 1;
            }
        }
    }
}
