using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRotator : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _rotationSpeed;

    [SerializeField] private Joystick _joystick;

    // Update is called once per frame
    void Update()
    {
        float singleStep = _rotationSpeed * Time.deltaTime;
        Vector3 direction = Vector3.forward * _joystick.Vertical + Vector3.right * _joystick.Horizontal;
        Vector3 rotation = Vector3.RotateTowards(transform.forward, direction, singleStep, 0.0f);

        direction = transform.TransformDirection(direction);
        direction *= _speed;

        transform.localRotation = Quaternion.Slerp(transform.localRotation, 
            Quaternion.LookRotation(rotation), 
            Time.deltaTime * _rotationSpeed);
    }
}
