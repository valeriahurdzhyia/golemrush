using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class DropPlayerInWaterController : DropInWaterController
{
    public MoveCamera Camera { get; set; }

    public override async void DropInWater(TeleporterController teleporter)
    {
        _effectDrownInWater.transform.SetParent(null);
        _effectDrownInWater.transform.localEulerAngles = _dropInWaterAngle;
        _effectDrownInWater.Play();
        MMVibrationManager.Haptic(HapticTypes.Failure);

        if (Camera != null)
        {
            Camera.SetActive(false);
        }

        Collider.isTrigger = true;

        await Task.Delay(_delayInMilliseconds);

        if (Camera != null)
        {
            Camera.SetActive(true);
        }

        Collider.isTrigger = false;

        teleporter.Teleport(Rigidbody);

        await Task.Delay(100);

        teleporter.Teleport(Rigidbody);

        await Task.Delay(_delayInMilliseconds);

        _effectDrownInWater.transform.SetParent(transform);
        _effectDrownInWater.transform.localPosition = _startParticleLocalPosition;
        _effectDrownInWater.transform.localEulerAngles = _startParticleLocalAngle;
    }
}
