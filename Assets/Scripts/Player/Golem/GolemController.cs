using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemController : MonoBehaviour, IEventListener<PlayerState>, IEventListener<PlayerPhase>
{
    [SerializeField] private OnPlayerChangeStateEvent _onPlayerChangeStateEvent;
    [SerializeField] private OnPlayerChangePhaseEvent _onPlayerChangePhaseEvent;

    [SerializeField] private CharacterAnimationController _characterAnimationController;

    [SerializeField] private Collider _handLeft;
    [SerializeField] private Collider _handRight;

    [SerializeField] private ParticleSystem _effectChangToIceState;
    [SerializeField] private ParticleSystem _effectChangToFireState;

    public ItemObject item;
    private AudioSource audioSource;

    private int _temperature;
    public int Temperature => _temperature;

    private SkinnedMeshRenderer[] _skinedMeshArray;

    [SerializeField] private PlayerState _playerState;
    public PlayerState PlayerState => _playerState;

    private void Awake()
    {
        _skinedMeshArray = GetComponentsInChildren<SkinnedMeshRenderer>();
    }
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnEnable()
    {
        _onPlayerChangeStateEvent.RegisterListener(this);
        _onPlayerChangePhaseEvent.RegisterListener(this);
    }

    void OnDisable()
    {
        _onPlayerChangeStateEvent.UnregisterListener(this);
        _onPlayerChangePhaseEvent.UnregisterListener(this);
    }

    public void OnEventRaised(PlayerState state)
    {
        _playerState = state;

        foreach(var skin in _skinedMeshArray)
        {
            skin.material = state.Material;
        }

        _temperature = state.Temperature;

        if(_temperature==1)
        {
            _effectChangToFireState.Play();
        }
        else if(_temperature==-1)
        {
            _effectChangToIceState.Play();
        }
        //Play AUDIO
        audioSource.Play();
    }
    public void OnEventRaised()
    {
        throw new System.NotImplementedException();
    }

    public void OnEventRaised(PlayerPhase eventData)
    {
        if(eventData == PlayerPhase.Attack)
        {
            _characterAnimationController.EnableAttack();
            _handLeft.enabled = true;
            _handRight.enabled = true;
        }
        else if(eventData == PlayerPhase.Collect)
        {
            _characterAnimationController.DoCollect();
        }
        else
        {
            _characterAnimationController.DisableAttack();
            _handLeft.enabled = false;
            _handRight.enabled = false;
        }
    }
}
