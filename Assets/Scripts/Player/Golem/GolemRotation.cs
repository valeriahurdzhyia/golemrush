using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;

public class GolemRotation : MonoBehaviour
{
    public bool IsAbleToMove { get; set; }
    [SerializeField] private float _rotationSpeed;
    [SerializeField] private Joystick _joystick;
    private float angle;
    public Joystick Joystick => _joystick;

    [SerializeField] private Transform _bodyHolder;


    public void Start()
    {
         angle = 0;
    }
    public void Update()
    {
        if (Joystick.Horizontal > 0)
        {
            angle = 90;
            angle -= 90 * Joystick.Vertical;
        }
        else if (Joystick.Horizontal < 0)
        {
            angle = -90;
            angle += 90 * Joystick.Vertical;
        }

        Vector3 v = new Vector3(0, angle, 0);
        _bodyHolder.localRotation = Quaternion.Slerp(_bodyHolder.localRotation, Quaternion.Euler(v), Time.deltaTime * _rotationSpeed);
    }
}
