using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StepsEffect : MonoBehaviour
{
    [SerializeField] private ParticleSystem _stepEffect;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            _stepEffect.Play();
        }
    }
}