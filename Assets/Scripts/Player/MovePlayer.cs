using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [SerializeField] private Joystick _joystick;
    public GameObject Planet;
    public float speed = 4;
    public float JumpHeight = 1.2f;

    float gravity = 100;
    bool OnGround = false;

    float distanceToGround;
    Vector3 Groundnormal;


    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.freezeRotation = true;
    }

    void Update()
    {
        // MOVEMENT

        //float x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        //float z = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        float x = _joystick.Horizontal * Time.deltaTime * speed;
        float z = _joystick.Vertical * Time.deltaTime * speed;

        transform.Translate(x, 0, z);

        // Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(transform.up * 40000 * JumpHeight * Time.deltaTime);
        }


        // Ground Control

        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(transform.position, -transform.up, out hit, 10))
        {
            distanceToGround = hit.distance;
            Groundnormal = hit.normal;

            if (distanceToGround <= 0.2f)
            {
                OnGround = true;
            }
            else
            {
                OnGround = false;
            }
        }

        // Gravity and Rotation

        Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;
        if (OnGround == false)
        {
            rb.AddForce(gravDirection * -gravity);
        }

        // 

        Quaternion toRotation = Quaternion.FromToRotation(transform.up, Groundnormal) * transform.rotation;
        transform.rotation = toRotation;

    }


    // Change planet

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.transform != Planet.transform)
        {
            Planet = collision.transform.gameObject;
            Vector3 gravDirection = (transform.position - Planet.transform.position).normalized;
            Quaternion toRotation = Quaternion.FromToRotation(transform.up, gravDirection) * transform.rotation;
            transform.rotation = toRotation;
            rb.velocity = Vector3.zero;
            rb.AddForce(gravDirection * gravity);
        }
    }
}
