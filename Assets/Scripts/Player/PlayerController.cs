using MoreMountains.NiceVibrations;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : CharacterController
{
    public const string PLAYER_TAG = "Player";
    public const string PLAYER_TAG_VISIBLE = "PlayerVisible";
    public const string PLAYER_AUDIOTRACKER_TAG = "PlayerAudioTracker";


    [SerializeField] private OnPlayerChangePhaseEvent _onPlayerChangePhaseEvent;
    [SerializeField] private Joystick _joystick;
    [SerializeField] protected MoveCamera _camera;
    [SerializeField] private GolemController _golemController;
    [SerializeField] private InventoryObject inventory;
    protected override void Awake()
    {
        base.Awake();
        (_dropInWaterController as DropPlayerInWaterController).Camera = _camera;
    }

    protected override void SetUpMoveDirection()
    {
        _moveDir = new Vector3(_joystick.Horizontal, 0, _joystick.Vertical).normalized;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == EnemyController.ENEMY_TAG)
        {
            if(other.transform.parent.GetComponent<EnemyController>().IsAlive)
            {
                _onPlayerChangePhaseEvent?.Raise(PlayerPhase.Attack);
            }           
        }
        else if(other.tag == Item.TAG_RESOURCE)
        {
            var item = other.GetComponent<Item>();
            inventory.AddItem(item.item, 1);

            item.PlayEffect();
            _onPlayerChangePhaseEvent?.Raise(PlayerPhase.Collect);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == EnemyController.ENEMY_TAG)
        {
            _onPlayerChangePhaseEvent?.Raise(PlayerPhase.Idle);
        }
    }

    protected override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        if(collision.transform.tag == _groundTag)
        {
            var territoryData = collision.transform.GetComponent<TerritoryDataController>()?.territoryData;

            if(territoryData != null)
            {
                if(territoryData.PlayerState.Temperature != _golemController.PlayerState.Temperature)
                {
                    if(territoryData.PlayerState.Temperature != 0)
                    {
                        _hitableController.StartPassiveDamage();
                    }                   
                }
                else if(territoryData.PlayerState.Temperature == _golemController.PlayerState.Temperature)
                {
                    _hitableController.StopPassiveDamage();
                }
            }
        }
    }

    protected override void DoReset()
    {
        base.DoReset();
        MMVibrationManager.Haptic(HapticTypes.Warning);
    }
}