using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlayerState", menuName = "Player/PlayerState")]
public class PlayerState : ScriptableObject
{
    [SerializeField] private Material _material;
    [SerializeField] private int _temperature;

    public Material Material => _material;
    public int Temperature => _temperature;
}
