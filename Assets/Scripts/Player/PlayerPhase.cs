using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerPhase
{
    Idle,
    Attack,
    Collect
}
